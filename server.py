import os
import sys
import re
import time
from serial import Serial
import serial.tools.list_ports
# from datetime import datetime
from functools import wraps

from flask import Flask, render_template, request, Response
from flask_socketio import SocketIO
from flask_sqlalchemy import SQLAlchemy

import gevent
import gevent.monkey
gevent.monkey.patch_all()
SERIAL_THREAD = None
SerialInterface = None
MAX_STEP = 30
GRAPH_DISPLAYED = 0
MAX_GRAPH = 4
NOTSTOP = False;
SHIFT = 30;

# Global variables
#
app = Flask(__name__, static_url_path='/static')
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode='gevent')

basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'database.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
def_freq = [5709, 5749, 5789, 5829]
bauds = ['9600', '19200', '38400', '57600', '115200', '230400','250000', '460800',
         '921600']
parities = ['None', 'Even', 'Odd', 'Mark', 'Space']
stopbits = ['1', '1.5', '2']
bytes = ['5', '6', '7', '8']
graph_bws = ['1', '2',  '5', '10']
graph_steps = ['240', '120', '60', '30']
numGraphs = 4
graphs = {'graph1': {'data': [], 'labels': []},
          'graph2': {'data': [], 'labels': []},
          'graph3': {'data': [], 'labels': []},
          'graph4': {'data': [], 'labels': []}}
# Database definition
#


class Config(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String(80), unique=True, nullable=False)
    value = db.Column(db.String(80), nullable=True)
    description = db.Column(db.String(80), nullable=True)


class Frequency(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    band = db.Column(db.Integer, nullable=False)
    channel = db.Column(db.Integer, nullable=False)
    frequency = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<Frequency %r>' % self.frequency


class Graph(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    central_frequency = db.Column(db.Integer)
    frequency = db.Column(db.Integer)
    timestamp = db.Column(db.DateTime)
    rssi_value = db.Column(db.Integer)
#
# Global utilities Functions
#


def check_auth(username, password):
    '''Check if a username password combination is valid.'''
    return username == 'admin' and password == getConfig('password')


def authenticate():
    '''Sends a 401 response that enables basic auth.'''
    return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated


def authenticated_only(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            disconnect()
        else:
            return f(*args, **kwargs)
    return wrapped


def server_log(message):
    '''Messages emitted from the server script.'''
    print (message)
    socketio.emit('hardware_log', message)


def db_init():

    db.create_all()  # Creates tables from database classes/models
    db_reset_frequencies()
    db_reset_default_password()
    db_reset_default_graph()
    db_reset_default_port()


def db_reset_default_password():
    createConfig('password', "vtxpower", 'admin password')


def db_reset_default_graph():
    createConfig('graph-central-frequency-1', '5740',
                 'central frequency for graph-1')
    createConfig('graph-central-frequency-2', '5780',
                 'central frequency for graph-2')
    createConfig('graph-central-frequency-3', '5820',
                 'central frequency for graph-3')
    createConfig('graph-central-frequency-4', '5860',
                 'central frequency for graph-4')
    createConfig('graph-color-1', '#ffff', 'color for graph-1')
    createConfig('graph-color-2', '#ff00', 'color for graph-2')
    createConfig('graph-color-3', '#00ff', 'color for graph-3')
    createConfig('graph-color-4', '#0f0f', 'color for graph-4')
    createConfig('graph_bws', '20', "Graph Bandwidth MHz")
    createConfig('graph_steps', '30', "Graph Bandwidth MHz")


def db_reset_frequencies():
    '''Resets database frequencies to default.'''
    db.session.query(Frequency).delete()
    # IMD Channels
    db.session.add(Frequency(band='IMD', channel='E2', frequency='5685'))
    db.session.add(Frequency(band='IMD', channel='F2', frequency='5760'))
    db.session.add(Frequency(band='IMD', channel='F4', frequency='5800'))
    db.session.add(Frequency(band='IMD', channel='F7', frequency='5860'))
    db.session.add(Frequency(band='IMD', channel='E6', frequency='5905'))
    db.session.add(Frequency(band='IMD', channel='E4', frequency='5645'))
    # Band C - Raceband
    db.session.add(Frequency(band='C', channel='C1', frequency='5658'))
    db.session.add(Frequency(band='C', channel='C2', frequency='5695'))
    db.session.add(Frequency(band='C', channel='C3', frequency='5732'))
    db.session.add(Frequency(band='C', channel='C4', frequency='5769'))
    db.session.add(Frequency(band='C', channel='C5', frequency='5806'))
    db.session.add(Frequency(band='C', channel='C6', frequency='5843'))
    db.session.add(Frequency(band='C', channel='C7', frequency='5880'))
    db.session.add(Frequency(band='C', channel='C8', frequency='5917'))
    # Band F - ImmersionRC, Iftron
    db.session.add(Frequency(band='F', channel='F1', frequency='5740'))
    db.session.add(Frequency(band='F', channel='F2', frequency='5760'))
    db.session.add(Frequency(band='F', channel='F3', frequency='5780'))
    db.session.add(Frequency(band='F', channel='F4', frequency='5800'))
    db.session.add(Frequency(band='F', channel='F5', frequency='5820'))
    db.session.add(Frequency(band='F', channel='F6', frequency='5840'))
    db.session.add(Frequency(band='F', channel='F7', frequency='5860'))
    db.session.add(Frequency(band='F', channel='F8', frequency='5880'))
    # Band E - HobbyKing, Foxtech
    db.session.add(Frequency(band='E', channel='E1', frequency='5705'))
    db.session.add(Frequency(band='E', channel='E2', frequency='5685'))
    db.session.add(Frequency(band='E', channel='E3', frequency='5665'))
    db.session.add(Frequency(band='E', channel='E4', frequency='5645'))
    db.session.add(Frequency(band='E', channel='E5', frequency='5885'))
    db.session.add(Frequency(band='E', channel='E6', frequency='5905'))
    db.session.add(Frequency(band='E', channel='E7', frequency='5925'))
    db.session.add(Frequency(band='E', channel='E8', frequency='5945'))
    # Band B - FlyCamOne Europe
    db.session.add(Frequency(band='B', channel='B1', frequency='5733'))
    db.session.add(Frequency(band='B', channel='B2', frequency='5752'))
    db.session.add(Frequency(band='B', channel='B3', frequency='5771'))
    db.session.add(Frequency(band='B', channel='B4', frequency='5790'))
    db.session.add(Frequency(band='B', channel='B5', frequency='5809'))
    db.session.add(Frequency(band='B', channel='B6', frequency='5828'))
    db.session.add(Frequency(band='B', channel='B7', frequency='5847'))
    db.session.add(Frequency(band='B', channel='B8', frequency='5866'))
    #
    # Band A - Team BlackSheep, RangeVideo, SpyHawk, FlyCamOne USA
    db.session.add(Frequency(band='A', channel='A1', frequency='5865'))
    db.session.add(Frequency(band='A', channel='A2', frequency='5845'))
    db.session.add(Frequency(band='A', channel='A3', frequency='5825'))
    db.session.add(Frequency(band='A', channel='A4', frequency='5805'))
    db.session.add(Frequency(band='A', channel='A5', frequency='5785'))
    db.session.add(Frequency(band='A', channel='A6', frequency='5765'))
    db.session.add(Frequency(band='A', channel='A7', frequency='5745'))
    db.session.add(Frequency(band='A', channel='A8', frequency='5725'))
    db.session.commit()
    server_log('Database frequencies reset')


def db_reset_default_port():
    createConfig('serial_port', "", 'serial port used to get data')
    createConfig("serial_baud_rate", "115200", 'serial baud rate')
    createConfig("serial_bit", "8", 'serial bit size')
    createConfig("serial_parity", "None", 'serial parity')
    createConfig("serial_stop_bit", "1", 'serial stop bit')

#
# Config management functions
#

# Config
#


def getConfig(p_key):
    config_item = Config.query.filter_by(key=p_key).first()
    return config_item.value


def createConfig(p_key, p_value, p_description):
    config_item = Config(key=p_key, value=p_value, description=p_description)
    db.session.add(config_item)
    db.session.commit()


def updateConfig(p_key, p_newvalue):
    config_item = Config.query.filter_by(key=p_key).first()
    config_item.value = p_newvalue
    db.session.commit()


#
# pages routing function
#


@app.route("/")
def index():
    graphcolors={}
    for i in range (1, numGraphs+1):
        graphcolors['graph-%d' % i] = getConfig('graph-color-%d' % i)
    return render_template('index.html', main=True, graphcolors=graphcolors)


@app.route("/settings")
@requires_auth
def settings():
    devices = list(serial.tools.list_ports.comports())
    ports = []
    for port in devices:
        ports.append(port.device)

    current_bws = getConfig('graph_bws')
    current_steps =getConfig('graph_steps')
    current_colors = []
    graph_central_frequencies = []

    for i in range(1, numGraphs+1):
        current_colors.append(getConfig('graph-color-%d' % i))
        graph_central_frequencies.append(getConfig(
                                         'graph-central-frequency-%d' % i))

    return render_template('settings.html',
                           current_port=getConfig('serial_port'),
                           current_baud=getConfig('serial_baud_rate'),
                           current_parity=getConfig('serial_parity'),
                           current_stopbit=getConfig('serial_stop_bit'),
                           current_bytesize=getConfig('serial_bit'),
                           current_bws=current_bws,
                           current_steps=current_steps,
                           current_colors=current_colors,
                           ports=ports,
                           bauds=bauds,
                           parities=parities,
                           stopbits=stopbits,
                           bytes=bytes,
                           graph_central_frequencies=graph_central_frequencies,
                           graph_bws=graph_bws,
                           graph_steps=graph_steps,
                           frequencies=Frequency,
                           num_graph=numGraphs)

#
# event handler Functions
#

# handle client connection


@socketio.on('connect')
def connect_handler():
    server_log('Client connected')
# handle starting vtx analyser


@socketio.on('start')
def start_analyzer():

    global SERIAL_THREAD
    global NOTSTOP
    NOTSTOP = not NOTSTOP
    if NOTSTOP == True:
        if SERIAL_THREAD is None:
           SERIAL_THREAD = gevent.spawn(serial_thread_function)
           server_log('Starting Analyzer')
        else:
           gevent.joinall([SERIAL_THREAD])
           server_log('Restarting Analyzer')
           SERIAL_THREAD = gevent.spawn(serial_thread_function)
    return


@socketio.on("set_c_frequency")
def set_c_frequency():
    return


@socketio.on("set_graph_color")
def set_graph_color():
    return


@socketio.on("set_serial_port")
@authenticated_only
def set_serial_port(data):
    server_log("set serial device:%s" % data)
    updateConfig('serial_port', data['port'])
    return


@socketio.on("set_serial_baud")
@authenticated_only
def set_serial_baud(data):
    server_log("set serial baud:%s" % data)
    updateConfig('serial_baud_rate', data['baud'])
    return


@socketio.on("set_serial_parity")
@authenticated_only
def set_serial_parity(data):
    server_log("set serial parity:%s" % data)
    updateConfig('serial_parity', data['parity'])
    return


@socketio.on("set_serial_stopbit")
@authenticated_only
def set_serial_stopbit(data):
    server_log("set serial stop bit:%s" % data)
    updateConfig('serial_stop_bit', data['stopbit'])
    return


@socketio.on("set_serial_bytesize")
@authenticated_only
def set_serial_bytersize(data):
    server_log("set serial byte size:%s" % data)
    updateConfig('serial_bit', data['byte'])
    return


@socketio.on("set_graph_frequency")
@authenticated_only
def set_graph_fequency(data):
    updateConfig('graph-central-frequency-%d' % data['graph'],
                 data['frequency'])
    return


@socketio.on("set_graph_color")
@authenticated_only
def set_serial_graph_color(data):
    server_log("set graph color:%s" % data)
    updateConfig('graph-color-%d' % data['graph'], '#'+data['color'])
    return


@socketio.on("set_graph_bw")
@authenticated_only
def set_serial_graph_bw(data):
    server_log("set graph bw:%s" % data)
    updateConfig('graph-bw-%d' % data['graph'], data['bw'])
    return


@socketio.on('graph_rendered')
def graph_rendered():
    global GRAPH_DISPLAYED
    if GRAPH_DISPLAYED < 4:
        GRAPH_DISPLAYED = GRAPH_DISPLAYED + 1
    return


@socketio.on("set_graph_step")
@authenticated_only
def set_serial_graph_step(data):
    server_log("set graph step:%s" % data)
    updateConfig('graph-step-%d' % data['graph'], data['step'])
    return


def serial_thread_function():
    global GRAPH_DISPLAYED
    global NOTSTOP
    values = {}
    step = 0
    server_log("serial_thread_funtion")
    open_serial()
    time.sleep(2)
    # send config.
    cval="c%s;%s;%s;%s" %(getConfig('graph-central-frequency-1'),
                         getConfig('graph-central-frequency-2'),
                         getConfig('graph-central-frequency-3'),
                         getConfig('graph-central-frequency-4'))
    server_log(cval.encode('ISO-8859-1'))
    try:
      SerialInterface.write(cval.encode('ISO-8859-1'))
    except serial.SerialTimeoutException:
      SerialInterface.close();
      return
    ack=""
    server_log('config send to arduino')
    while not re.match('^A.*', ack):
         (ack,endline)=read_serial().split("\n")
         server_log(ack)

    server_log("ack received")
    while NOTSTOP:
        while step != MAX_STEP:
            SerialInterface.write(b'n\n')
            #server_log("write")
            (values, endline) = read_serial().split("\n")
            #server_log("read")
            #server_log(values)
            if (step == 0):
                while not re.match('^0;.*', values):
                    SerialInterface.write(b'n\n')
                    #server_log("write")
                    values = read_serial()
                    #server_log("read")
                    #server_log(values)
                    step = 0
            splits = values.split(';')
            if (len(splits) != 5):
                server_log(values)
                continue
            (steps, v1, v2, v3, v4) = splits
            graphs['graph1']['data'].append(int(v1))
            graphs['graph1']['labels'].append("%d" % ((int(getConfig('graph-central-frequency-1'))-SHIFT)+int(steps)*2))
            graphs['graph2']['data'].append(int(v2))
            graphs['graph2']['labels'].append("%d" % ((int(getConfig('graph-central-frequency-2'))-SHIFT)+int(steps)*2))
            graphs['graph3']['data'].append(int(v3))
            graphs['graph3']['labels'].append("%d" % ((int(getConfig('graph-central-frequency-3'))-SHIFT)+int(steps)*2))
            graphs['graph4']['data'].append(int(v4))
            graphs['graph4']['labels'].append("%d" % ((int(getConfig('graph-central-frequency-4'))-SHIFT)+int(steps)*2))
            step = step + 1
        step = 0
        socketio.emit('graph_update', graphs)
        while GRAPH_DISPLAYED != 4:
            gevent.sleep(0)
        GRAPH_DISPLAYED = 0
        step = 0
        for i in range (1, numGraphs+1):
         graphs['graph%d' % i]['data'] = []
         graphs['graph%d' %i]['labels'] = []
        # time.sleep(1)

    return

def open_serial():
    global SerialInterface
    if SerialInterface is not None:
        SerialInterface.close()
    SerialInterface = serial.Serial()
    SerialInterface.baudrate = int(getConfig('serial_baud_rate'))
    SerialInterface.port = getConfig('serial_port')
    SerialInterface.write_timeout=1
    SerialInterface.open()
    SerialInterface.flush()

    return 0


def read_serial():
    line = SerialInterface.readline().decode('ISO-8859-1')
    return line

# main Program


if not os.path.exists('database.db'):
    db_init()


if __name__ == '__main__':
    socketio.run(app, host="0.0.0.0", debug=True)
