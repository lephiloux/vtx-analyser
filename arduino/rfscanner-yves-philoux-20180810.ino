#include <Arduino.h>
#include <SPI.h>

#define SPI_ADDRESS_SYNTH_A 0x01
#define SPI_ADDRESS_SYNTH_R 0x00
#define SPI_ADDRESS_POWER 0x0A
#define PIN_LED 13
#define PIN_SPI_DATA 6//11
#define PIN_SPI_SLAVE_SELECT 2//10
#define PIN_SPI_SLAVE_SELECT_2 3//10
#define PIN_SPI_SLAVE_SELECT_3 4//10
#define PIN_SPI_SLAVE_SELECT_4 5//10
#define PIN_SPI_CLOCK 9

#define PIN_RSSI_A A9
#define PIN_RSSI_B A8
#define PIN_RSSI_C A7
#define PIN_RSSI_D A6
#define delta_min  25
#define Device_1 // 1 2 3 4

#define BAUD_RATE 250000
#define START_F 5600

uint8_t module=1;
uint8_t delta_t = 0;
uint16_t rf_reg_NA[4]= {0,0,0,0};
uint16_t frequency[4]= {5709,5749,5789,5829};
uint8_t step=0;
String buffer;
String F1,F2,F3,F4;
char value[30];

//unsigned int delta_1 = 0;
//unsigned int old_delta_1 = 0;

float RSSI_A_VAL= 0;
float RSSI_B_VAL= 0;
float RSSI_C_VAL= 0;
float RSSI_D_VAL= 0;
float RSSI_A_VAL_OLD = 0;
float RSSI_B_VAL_OLD = 0;
float RSSI_C_VAL_OLD = 0;
float RSSI_D_VAL_OLD = 0;

int rf_reg_R=0x0008;

uint16_t vtxHexTable[]={0x2800,0x2800,0x2801,0x2801,0x2802,0x2802,0x2803,
  0x2803,0x2804,0x2804,0x2805,0x2805,0x2806,0x2806,0x2807,0x2807,0x2808,0x2808,
  0x2809,0x2809,0x280a,0x280a,0x280b,0x280b,0x280c,0x280c,0x280d,0x280d,0x280e,
  0x280e,0x280f,0x280f,0x2810,0x2810,0x2811,0x2811,0x2812,0x2812,0x2813,0x2813,
  0x2814,0x2814,0x2815,0x2815,0x2816,0x2816,0x2817,0x2817,0x2818,0x2818,0x2819,
  0x2819,0x281a,0x281a,0x281b,0x281b,0x281c,0x281c,0x281d,0x281d,0x281e,0x281e,
  0x281f,0x281f,0x2880,0x2880,0x2881,0x2881,0x2882,0x2882,0x2883,0x2883,0x2884,
  0x2884,0x2885,0x2885,0x2886,0x2886,0x2887,0x2887,0x2888,0x2888,0x2889,0x2889,
  0x288a,0x288a,0x288b,0x288b,0x288c,0x288c,0x288d,0x288d,0x288e,0x288e,0x288f,
  0x288f,0x2890,0x2890,0x2891,0x2891,0x2892,0x2892,0x2893,0x2893,0x2894,0x2894,
  0x2895,0x2895,0x2896,0x2896,0x2897,0x2897,0x2898,0x2898,0x2899,0x2899,0x289a,
  0x289a,0x289b,0x289b,0x289c,0x289c,0x289d,0x289d,0x289e,0x289e,0x289f,0x289f,
  0x2900,0x2900,0x2901,0x2901,0x2902,0x2902,0x2903,0x2903,0x2904,0x2904,0x2905,
  0x2905,0x2906,0x2906,0x2907,0x2907,0x2908,0x2908,0x2909,0x2909,0x290a,0x290a,
  0x290b,0x290b,0x290c,0x290c,0x290d,0x290d,0x290e,0x290e,0x290f,0x290f,0x2910,
  0x2910,0x2911,0x2911,0x2912,0x2912,0x2913,0x2913,0x2914,0x2914,0x2915,0x2915,
  0x2916,0x2916,0x2917,0x2917,0x2918,0x2918,0x2919,0x2919,0x291a,0x291a,0x291b,
  0x291b,0x291c,0x291c,0x291d,0x291d,0x291e,0x291e,0x291f,0x291f,0x2980,0x2980,
  0x2981,0x2981,0x2982,0x2982,0x2983,0x2983,0x2984,0x2984,0x2985,0x2985,0x2986,
  0x2986,0x2987,0x2987,0x2988,0x2988,0x2989,0x2989,0x298a,0x298a,0x298b,0x298b,
  0x298c,0x298c,0x298d,0x298d,0x298e,0x298e,0x298f,0x298f,0x2990,0x2990,0x2991,
  0x2991,0x2992,0x2992,0x2993,0x2993,0x2994,0x2994,0x2995,0x2995,0x2996,0x2996,
  0x2997,0x2997,0x2998,0x2998,0x2999,0x2999,0x299a,0x299a,0x299b,0x299b,0x299c,
  0x299c,0x299d,0x299d,0x299e,0x299e,0x299f,0x299f,0x2a00,0x2a00,0x2a01,0x2a01,
  0x2a02,0x2a02,0x2a03,0x2a03,0x2a04,0x2a04,0x2a05,0x2a05,0x2a06,0x2a06,0x2a07,
  0x2a07,0x2a08,0x2a08,0x2a09,0x2a09,0x2a0a,0x2a0a,0x2a0b,0x2a0b,0x2a0c,0x2a0c,
  0x2a0d,0x2a0d,0x2a0e,0x2a0e,0x2a0f,0x2a0f,0x2a10,0x2a10,0x2a11,0x2a11,0x2a12,
  0x2a12,0x2a13,0x2a13,0x2a14,0x2a14,0x2a15,0x2a15,0x2a16,0x2a16,0x2a17,0x2a17,
  0x2a18,0x2a18,0x2a19,0x2a19,0x2a1a,0x2a1a,0x2a1b,0x2a1b,0x2a1c,0x2a1c,0x2a1d,
  0x2a1d,0x2a1e,0x2a1e,0x2a1f,0x2a1f,0x2a80,0x2a80,0x2a81,0x2a81,0x2a82,0x2a82,
  0x2a83,0x2a83,0x2a84,0x2a84,0x2a85,0x2a85,0x2a86,0x2a86,0x2a87,0x2a87,0x2a88,
  0x2a88,0x2a89,0x2a89,0x2a8a,0x2a8a,0x2a8b,0x2a8b,0x2a8c,0x2a8c,0x2a8d,0x2a8d,
  0x2a8e,0x2a8e,0x2a8f};

/*int vtxFreqTable[] = {
  5865, 5845, 5825, 5805, 5785, 5765, 5745, 5725, // Band A
  5733, 5752, 5771, 5790, 5809, 5828, 5847, 5866, // Band B
  5705, 5685, 5665, 5645, 5885, 5905, 5925, 5945, // Band E
  5740, 5760, 5780, 5800, 5820, 5840, 5860, 5880, // Band F
  5658, 5695, 5732, 5769, 5806, 5843, 5880, 5917  // Band C / Raceband
};*/
/*uint16_t vtxHexTable[] = {
  0x2A05, 0x299B, 0x2991, 0x2987, 0x291D, 0x2913, 0x2909, 0x289F, // Band A
  0x2903, 0x290C, 0x2916, 0x291F, 0x2989, 0x2992, 0x299C, 0x2A05, // Band B
  0x2895, 0x288B, 0x2881, 0x2817, 0x2A0F, 0x2A19, 0x2A83, 0x2A8D, // Band E
  0x2906, 0x2910, 0x291A, 0x2984, 0x298E, 0x2998, 0x2A02, 0x2A0C, // Band F
  0x281D, 0x288F, 0x2902, 0x2914, 0x2987, 0x2999, 0x2A0C, 0x2A1E  // Band C / Raceband
};*/

uint16_t get_NA_value(int frequency) {
	uint16_t index; // Find the index in the frequency lookup table
  index = START_F-frequency;
	return vtxHexTable[index];
}

void config_f(int f1,int f2,int f3,int f4){

}
inline void sendBit(uint8_t value) {
    digitalWrite(PIN_SPI_CLOCK, LOW);
    delayMicroseconds(1);

    digitalWrite(PIN_SPI_DATA, value);
    delayMicroseconds(1);
    digitalWrite(PIN_SPI_CLOCK, HIGH);
    delayMicroseconds(1);

    digitalWrite(PIN_SPI_CLOCK, LOW);
    delayMicroseconds(1);
}
inline void sendBits(uint32_t bits, uint8_t count) {
    for (uint8_t i = 0; i < count; i++) {
        sendBit(bits & 0x1);
        bits >>= 1;
    }
}

inline void sendSlaveSelect(uint8_t value) {
  switch(module){
    case 1 : digitalWrite(PIN_SPI_SLAVE_SELECT, value);
              break;
    case 2 : digitalWrite(PIN_SPI_SLAVE_SELECT_2, value);
              break;
    case 3 : digitalWrite(PIN_SPI_SLAVE_SELECT_3, value);
              break;
    case 4 : digitalWrite(PIN_SPI_SLAVE_SELECT_4, value);
              break;}
    delayMicroseconds(1);
}

inline void sendRegister(uint8_t address, uint32_t data) {
    sendSlaveSelect(LOW);

    sendBits(address, 4);
    sendBit(HIGH); // Enable write.

    sendBits(data, 20);

    // Finished clocking data in
    sendSlaveSelect(HIGH);
    digitalWrite(PIN_SPI_CLOCK, LOW);
    digitalWrite(PIN_SPI_DATA, LOW);
}


void setSynthRegisterB(uint16_t value) {
        sendRegister(SPI_ADDRESS_SYNTH_A, value);
}

void setPowerDownRegister(uint32_t value) {
    sendRegister(SPI_ADDRESS_POWER, value);
}

void setup() {
    // put your setup code here, to run once:
    //  Set serial speed
    Serial.begin(BAUD_RATE);
    //ADC resolution
    analogReadResolution(13);
    delay(1000);
    // set delay between read
    delta_t = 50;
    // init io(s) pins
    pinMode(PIN_LED, OUTPUT);
    // analogue pins to read rssi from rx modules
    pinMode(PIN_RSSI_A, INPUT);
    pinMode(PIN_RSSI_B, INPUT);
    pinMode(PIN_RSSI_C, INPUT);
    pinMode(PIN_RSSI_D, INPUT);
    // define chip select pin for spi bus for each rx modules
    pinMode(PIN_SPI_SLAVE_SELECT, OUTPUT);
    pinMode(PIN_SPI_SLAVE_SELECT_2, OUTPUT);
    pinMode(PIN_SPI_SLAVE_SELECT_3, OUTPUT);
    pinMode(PIN_SPI_SLAVE_SELECT_4, OUTPUT);
    // define clock and data pin for SPI bus
    pinMode(PIN_SPI_DATA, OUTPUT);
    pinMode(PIN_SPI_CLOCK, OUTPUT);
    // init SPI bus
    digitalWrite(PIN_SPI_SLAVE_SELECT, HIGH);
    digitalWrite(PIN_SPI_SLAVE_SELECT_2, HIGH);
    digitalWrite(PIN_SPI_SLAVE_SELECT_3, HIGH);
    digitalWrite(PIN_SPI_SLAVE_SELECT_4, HIGH);
    digitalWrite(PIN_SPI_CLOCK, LOW);
    digitalWrite(PIN_SPI_DATA, LOW);
    digitalWrite(PIN_LED, HIGH);
    //init power register of each rx module
    for (char i=1;i<=4;i++){
      module=i;
      setPowerDownRegister(0b00010000110111110011);
    }
    delay(50);
    sendRegister(SPI_ADDRESS_SYNTH_R, rf_reg_R);

    // initial frequency set to fatshark 1 3 5 7
    for (int i=0; i<4; i++){
      rf_reg_NA[i] = get_NA_value(frequency[i]);
      module =i+1; sendRegister(SPI_ADDRESS_SYNTH_A,rf_reg_NA[i]);
    }

  }


void loop() {
  uint8_t idx,idx2;
  // wait for serial input from web gui
  while (Serial.available() == 0);
  // serial is available
  buffer=Serial.readStringUntil('\n');
    // gui ask for next rssi values

  if (buffer[0]=='n'){
    //if((millis()-old_delta_1)> delta_t){
      //old_delta_1 = millis();
      RSSI_A_VAL =0;
      RSSI_B_VAL =0;
      RSSI_C_VAL =0;
      RSSI_D_VAL =0;
      for (int i =0;i<40;i++){
      RSSI_A_VAL +=analogRead(PIN_RSSI_A)/10;///4095.0*3.3;delay(1);
      RSSI_B_VAL +=analogRead(PIN_RSSI_B)/10;
      RSSI_C_VAL +=analogRead(PIN_RSSI_C)/10;
      RSSI_D_VAL +=analogRead(PIN_RSSI_D)/10;
      }
      RSSI_A_VAL /=40;
      RSSI_B_VAL /=40;
      RSSI_C_VAL /=40;
      RSSI_D_VAL /=40;
      step++;

      for (int i = 0 ; i<4; i++){
        if ( step < 31)
           rf_reg_NA[i]+=2;
        else
           {
            // restart with first regster values
            rf_reg_NA[i]=get_NA_value(frequency[i]);
            step = 0 ;
           }
           
        module =i+1; sendRegister(SPI_ADDRESS_SYNTH_A,rf_reg_NA[i]);
      }
      Serial.println(String(step)+";"+String(RSSI_A_VAL*10)+";"+String(RSSI_B_VAL*10)+";"+String(RSSI_C_VAL*10)+";"+String(RSSI_D_VAL*10));
      delay(delta_t);
    //}
  }
  // gui provide new set of frequency
  else {
    if (buffer[0]=='c'){
          idx=buffer.indexOf(';',1);
          F1=buffer.substring(1,idx);
          frequency[0]=F1.toInt()-30;
          idx2=buffer.indexOf(';',idx+1);
          F2=buffer.substring(idx+1,idx2);
          frequency[1]=F2.toInt()-30;
          idx=buffer.indexOf(';',idx2+1);
          F3=buffer.substring(idx2+1,idx);
          frequency[2]=F3.toInt()-30;
          idx2=buffer.indexOf(';',idx+1);
          F4=buffer.substring(idx+1,idx2);
          frequency[3]=F4.toInt()-30;
          for (int i=0; i<4; i++)
           {
             rf_reg_NA[i]=get_NA_value(frequency[i]);
             module =i+1; sendRegister(SPI_ADDRESS_SYNTH_A,rf_reg_NA[i]);
           }
          delay(50);
          Serial.println("A\n");
      }
  }
}
